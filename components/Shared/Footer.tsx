import { Divider, Input } from 'antd';
import {
  Container,
  StyledColumn,
  StyledColumns,
  StyledFooter,
  StyledIconsContainer,
  StyledLink,
  StyledLinksHeaderText,
  StyledRightsText,
  StyledSpace,
  StyledSubFooter,
  StyledSubscribeSubText,
  SubscribeButton,
  SubscribeInputContainer,
} from './elements';
import {
  FacebookFilled,
  InstagramOutlined,
  TwitterOutlined,
  GithubOutlined,
} from '@ant-design/icons';

export const Footer: React.FC = () => {
  return (
    <Container>
      <StyledFooter>
        <StyledSpace direction="vertical" split={<Divider type="horizontal" />}>
          <StyledColumns>
            <StyledColumn>
              <StyledLinksHeaderText>SOLUTIONS</StyledLinksHeaderText>
              <StyledLink href="/marketing" label="Marketing" />
              <StyledLink href="/analytics" label="Analytics" />
              <StyledLink href="/commerce" label="Commerce" />
              <StyledLink href="/insights" label="Insights" />
            </StyledColumn>
            <StyledColumn>
              <StyledLinksHeaderText>SUPPORT</StyledLinksHeaderText>
              <StyledLink href="/marketing" label="Marketing" />
              <StyledLink href="/analytics" label="Analytics" />
              <StyledLink href="/commerce" label="Commerce" />
              <StyledLink href="/insights" label="Insights" />
            </StyledColumn>
            <StyledColumn>
              <StyledLinksHeaderText>COMPANY</StyledLinksHeaderText>
              <StyledLink href="/about" label="About" />
              <StyledLink href="/blogs" label="Blogs" />
              <StyledLink href="/jobs" label="Jobs" />
              <StyledLink href="/press" label="Press" />
              <StyledLink href="/partners" label="Partners" />
            </StyledColumn>
            <StyledColumn>
              <StyledLinksHeaderText>LEGAL</StyledLinksHeaderText>
              <StyledLink href="/claim" label="Claim" />
              <StyledLink href="/privacy" label="Privacy" />
              <StyledLink href="/terms" label="Terms" />
            </StyledColumn>
            <StyledColumn className="subscribe">
              <StyledLinksHeaderText>
                SUBSCRIBE TO OUR NEWSLETTER
              </StyledLinksHeaderText>
              <StyledSubscribeSubText>
                The latest news, articles, and resources, sent to your inbox
                weekly.
              </StyledSubscribeSubText>
              <SubscribeInputContainer>
                <Input placeholder="Enter your email" />
                <SubscribeButton size="large">Subscribe</SubscribeButton>
              </SubscribeInputContainer>
            </StyledColumn>
          </StyledColumns>
          <StyledSubFooter>
            <StyledRightsText>
              2020 Workflow, Inc. All rights reserved
            </StyledRightsText>
            <StyledIconsContainer>
              <FacebookFilled />
              <InstagramOutlined />
              <TwitterOutlined />
              <GithubOutlined />
            </StyledIconsContainer>
          </StyledSubFooter>
        </StyledSpace>
      </StyledFooter>
    </Container>
  );
};
