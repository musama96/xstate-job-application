import { BankFilled } from '@ant-design/icons';
import {
  StyledHeading,
  StyledFeaturesSection,
  StyledRegularText,
  FeaturesContainer,
  IconContainer,
  StyledSmallerHeading,
  StyledSmallText,
  StyledCard,
} from './elements';

export const FeaturesSection: React.FC = () => {
  return (
    <StyledFeaturesSection>
      <StyledHeading>Inbox support built for efficiency</StyledHeading>
      <StyledRegularText>
        Iste numquam doloribus est perferendis expedita. Blanditiis sit esse
        ipsa sunt ratione aperiam autem.
      </StyledRegularText>
      <StyledRegularText>
        Sunt quia eveniet totam vitae dignissimos debitis sed quos eum. Et ea
        itaque doloremque.
      </StyledRegularText>

      <FeaturesContainer>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
      </FeaturesContainer>
      <FeaturesContainer>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
        <StyledCard direction="vertical">
          <IconContainer>
            <BankFilled style={{ fontSize: '24px', color: 'white' }} />
          </IconContainer>
          <StyledSmallerHeading>Unlimited Inboxes</StyledSmallerHeading>
          <StyledSmallText>
            Aliquam minus autem laboriosam. Perferendis enim quasi quia unde
            fugit. Velit fugit et totam sed eos voluptas sunt voluptas.
          </StyledSmallText>
        </StyledCard>
      </FeaturesContainer>
    </StyledFeaturesSection>
  );
};
