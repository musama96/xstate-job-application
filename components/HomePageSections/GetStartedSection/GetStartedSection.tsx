import { Space } from 'antd';
import {
  GetStartedButton,
  LearnMoreButton,
  StyledGetStartedSection,
  StyledHeaderText,
  StyledLeftContainer,
  StyledRightContainer,
} from './elements';

export const GetStartedSection: React.FC = () => {
  return (
    <>
      <StyledGetStartedSection>
        <StyledLeftContainer>
          <StyledHeaderText style={{ color: 'black' }}>
            Ready to get started?
          </StyledHeaderText>
          <StyledHeaderText>
            Get in touch or create an account.
          </StyledHeaderText>
        </StyledLeftContainer>
        <StyledRightContainer>
          <Space>
            <LearnMoreButton size="large">Learn More</LearnMoreButton>
            <GetStartedButton size="large">Get Started</GetStartedButton>
          </Space>
        </StyledRightContainer>
      </StyledGetStartedSection>
    </>
  );
};
