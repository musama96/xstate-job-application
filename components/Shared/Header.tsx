import {
  StyledHeader,
  StyledLogo,
  StyledDropdown,
  StyledNav,
  StyledAuthNav,
  SignInButton,
  SignUpButton,
} from './elements';
import Link from 'next/link';
import Image from 'next/image';
import { MenuOption } from '@/types';
import { Container } from '@/components';
import { DownOutlined } from '@ant-design/icons';
import { Menu } from 'antd';

const options: MenuOption[] = [
  { label: 'Option 1', url: 'https://google.com' },
  { label: 'Option 2', url: 'https://google.com' },
];

export const Header: React.FC = () => {
  return (
    <Container>
      <StyledHeader>
        <StyledLogo>
          <Image
            layout="fill"
            objectFit="contain"
            src="/logo.png"
            alt="Site logo"
          />
        </StyledLogo>
        <StyledNav>
          <StyledDropdown
            overlay={
              <Menu>
                {options.map((option, index) => (
                  <Menu.Item key={index}>
                    <a href={option.url}>{option.label}</a>
                  </Menu.Item>
                ))}
              </Menu>
            }
          >
            <a
              className="ant-dropdown-link"
              onClick={(e) => e.preventDefault()}
            >
              Solutions <DownOutlined />
            </a>
          </StyledDropdown>
          <Link href="/pricing">Pricing</Link>
          <Link href="/partners">Partners</Link>
          <Link href="/company">Company</Link>
        </StyledNav>
        <StyledAuthNav>
          <SignInButton />
          <SignUpButton />
        </StyledAuthNav>
      </StyledHeader>
    </Container>
  );
};
