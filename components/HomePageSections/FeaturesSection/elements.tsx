import { Space } from 'antd';
import styled from 'styled-components';

export const StyledFeaturesSection = styled.section`
  background-image: linear-gradient(to right, #5b20b6, #4438ca);
  margin: 30px -30px 0 -30px;
  padding: 60px 30px;
`;

export const StyledHeading = styled.h1`
  color: white;
  font-weight: 600;
  font-size: 36px;
`;

export const StyledSmallerHeading = styled.p`
  color: white;
  margin: 0;
  font-size: 22px;
`;

export const StyledRegularText = styled.p`
  color: #dacefb;
  font-weight: 300;
  margin: 0;
  font-size: 18px;
`;

export const StyledSmallText = styled.p`
  color: #dacefb;
  font-weight: 300;
  font-size: 16px;
  margin: 0;
`;

export const FeaturesContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-top: 30px;
`;

export const IconContainer = styled.div`
  background-color: rgba(255, 255, 255, 0.1);
  padding: 8px;
  border-radius: 5px;
  height: 42.5px;
  width: 42.5px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const StyledCard = styled(Space)`
  max-width: 250px;
`;
