/** @type {import('next').NextConfig} */
const withAntdLess = require('next-plugin-antd-less');

module.exports = {
  reactStrictMode: true,
};

module.exports = withAntdLess({
  webpack(config) {
    return config;
  },
});
