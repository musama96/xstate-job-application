import styled from 'styled-components';
import { Dropdown, Button, Space } from 'antd';
import Link from 'next/link';
import { LinkProps } from '@/types';

export const Container = styled.div`
  margin: 30px 30px 0 30px;
`;

export const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 50px;
  font-size: 18px;
`;

export const StyledLogo = styled.div`
  height: 50px;
  width: 50px;
  position: relative;
`;

export const StyledDropdown = styled(Dropdown)`
  &.ant-dropdown-trigger {
    color: #6f7586;
  }
`;

export const StyledNav = styled.div`
  a {
    padding: 0 20px;
    color: #6f7586;
  }

  a:hover {
    text-decoration-line: underline;
  }
`;

export const StyledAuthNav = styled.div`
  & button {
    font-size: 18px;
    line-height: 20px;
  }

  & .sign-in {
    color: #6f7586;
  }

  & .sign-up {
    background-image: linear-gradient(to right, #773beb, #5045e5);
    border: 0;
    border-radius: 5px;
  }
`;

export const SignInButton = styled(() => (
  <Button className="sign-in" size="large" type="text">
    Sign In
  </Button>
))``;

export const SignUpButton = styled(() => (
  <Button className="sign-up" size="large" type="primary">
    Sign Up
  </Button>
))``;

export const StyledFooter = styled.section`
  margin-top: -30px;
  padding-bottom: 30px;
`;

export const StyledSpace = styled(Space)`
  display: flex;
`;

export const StyledColumns = styled.div`
  display: flex;
  justify-content: space-between;

  .subscribe {
    width: 300px;
  }
`;

export const StyledColumn = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;

  & a {
    color: #898c93;
    font-size: 18px;
    font-weight: 300;
    padding: 8px 0;
  }
`;

export const StyledLinksHeaderText = styled.p`
  color: #99a1ac;
  font-weight: 500;
  font-size: 18px;
  margin: 0;
  padding-bottom: 10px;
`;

export const StyledSubscribeSubText = styled.p`
  color: #898c93;
  font-weight: 300;
  font-size: 18px;
  margin: 0;
  padding-bottom: 10px;
`;

export const StyledLink = styled(({ href, label }: LinkProps) => (
  <Link href={href}>{label}</Link>
))``;

export const SubscribeInputContainer = styled.div`
  display: flex;

  input {
    border-radius: 5px;
  }
`;

export const SubscribeButton = styled(Button)`
  background-image: linear-gradient(to right, #773beb, #5045e5);
  border: 0;
  border-radius: 5px;
  color: white;
  margin-left: 10px;
`;

export const StyledSubFooter = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const StyledRightsText = styled.p`
  color: #989da3;
  margin: 0;
  font-size: 18px;
`;

export const StyledIconsContainer = styled.div`
  font-size: 24px;
  color: #9aa0ac;

  & > span {
    margin-left: 15px;
  }
`;
