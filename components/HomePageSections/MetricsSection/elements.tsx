import styled from 'styled-components';

export const StyledMetricsSection = styled.section`
  background-image: linear-gradient(to right, #111828, #3c424e);
  margin: 0 -30px;
  padding: 60px 30px;
`;

export const StyledContainer = styled.div`
  width: 50%;
`;

export const StyledSubHeaderText = styled.p`
  color: #9587ae;
  font-size: 16px;
`;

export const StyledHeaderText = styled.h1`
  color: white;
  font-weight: bold;
  font-size: 36px;
`;

export const StyledRegularText = styled.p`
  color: #c8cfd7;
  font-size: 18px;
`;

export const StyledMetrics = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export const StyledMetric = styled.div`
  display: flex;
  flex-direction: column;
  width: 48%;
`;
