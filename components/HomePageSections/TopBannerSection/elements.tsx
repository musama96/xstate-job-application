import { Button } from 'antd';
import styled from 'styled-components';

export const StyledTopBannerSection = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 450px;
  background-image: linear-gradient(to right, #5800dfab, #181242);
  border-radius: 10px;
`;

export const StyledTitleText = styled.h1`
  font-size: 56px;
  line-height: 60px;
  font-weight: 600;
  max-width: 500px;
  text-align: center;
  color: rgba(255, 255, 255, 0.8);
`;
export const StyledRegularText = styled.p`
  text-align: center;
  max-width: 700px;
  color: rgba(255, 255, 255, 0.8);
  font-size: 20px;
`;

export const StyledButtonsContainer = styled.div`
  button {
    margin: 0 10px;
    border: 0;
  }

  .live-demo {
    background-image: linear-gradient(to right, #773beb, #5045e5);
  }
`;

export const GetStartedButton = styled(() => (
  <Button size="large">Get Started</Button>
))``;

export const LiveDemoButton = styled(() => (
  <Button className="live-demo" type="primary" size="large">
    Live Demo
  </Button>
))``;
