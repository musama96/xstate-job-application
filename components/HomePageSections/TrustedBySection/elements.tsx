import styled from 'styled-components';

export const StyledTrustedBySection = styled.section`
  margin-top: 50px;
`;

export const StyledRegularText = styled.p`
  color: #777b87;
  text-align: center;
  font-size: 12px;
  font-weight: 500;
`;

export const LogosContainer = styled.div`
  display: flex;
`;

export const StyledLogo = styled.div`
  height: 40px;
  position: relative;
  flex-grow: 1;
`;
