import type { NextPage } from 'next';
import Head from 'next/head';
import {
  Container,
  DesignSection,
  TopBannerSection,
  TrustedBySection,
  FeaturesSection,
  MetricsSection,
  GetStartedSection,
} from '@/components';

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>XState Job Application</title>
        <meta
          name="description"
          content="XState job application landing page"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <TopBannerSection />
        <TrustedBySection />
        <DesignSection />
        <FeaturesSection />
        <MetricsSection />
        <GetStartedSection />
      </Container>
    </>
  );
};

export default Home;
