import {
  LogosContainer,
  StyledRegularText,
  StyledLogo,
  StyledTrustedBySection,
} from './elements';
import Image from 'next/image';

export const TrustedBySection: React.FC = () => {
  return (
    <StyledTrustedBySection>
      <StyledRegularText>
        TRUSTED BY OVER 5 VERY AVERAGE SMALL BUSINESSES
      </StyledRegularText>
      <LogosContainer>
        <StyledLogo>
          <Image
            layout="fill"
            objectFit="contain"
            src={'/formspree.svg'}
            alt={'Formspree logo'}
          />
        </StyledLogo>
        <StyledLogo>
          <Image
            layout="fill"
            objectFit="contain"
            src={'/formspree.svg'}
            alt={'Formspree logo'}
          />
        </StyledLogo>
        <StyledLogo>
          <Image
            layout="fill"
            objectFit="contain"
            src={'/formspree.svg'}
            alt={'Formspree logo'}
          />
        </StyledLogo>
        <StyledLogo>
          <Image
            layout="fill"
            objectFit="contain"
            src={'/formspree.svg'}
            alt={'Formspree logo'}
          />
        </StyledLogo>
      </LogosContainer>
    </StyledTrustedBySection>
  );
};
