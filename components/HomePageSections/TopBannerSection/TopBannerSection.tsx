import {
  GetStartedButton,
  LiveDemoButton,
  StyledTopBannerSection,
  StyledButtonsContainer,
  StyledRegularText,
  StyledTitleText,
} from './elements';

export const TopBannerSection: React.FC = () => {
  return (
    <>
      <StyledTopBannerSection>
        <StyledTitleText>Take control of your customer support</StyledTitleText>
        <StyledRegularText>
          Distinctio veritatis voluptatem possimus nobis molestias voluptates
          odio facilis aspernatur. Ipsum similique a commodi quaerat sint vitae.
        </StyledRegularText>
        <StyledButtonsContainer>
          <GetStartedButton />
          <LiveDemoButton />
        </StyledButtonsContainer>
      </StyledTopBannerSection>
    </>
  );
};
