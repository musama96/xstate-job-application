import { Space, Divider, Avatar } from 'antd';
import { BankTwoTone } from '@ant-design/icons';
import {
  StyledDesignSection,
  GetStartedButton,
  StyledCommentText,
  StyledContainer,
  StyledRightDesignDiv,
  StyledLeftDesignDiv,
  StyledHeading,
  StyledIntroSection,
  StyledNameDesgination,
  StyledSpace,
  StyledDesignDescription,
} from './elements';

export const DesignSection: React.FC = () => {
  return (
    <StyledDesignSection>
      <StyledContainer>
        <StyledSpace direction="vertical" split={<Divider type="horizontal" />}>
          <Space direction="vertical">
            <BankTwoTone style={{ fontSize: '24px' }} twoToneColor="#874ed2" />
            <StyledHeading>Stay on top of customer support</StyledHeading>
            <StyledDesignDescription>
              Quia suscipit voluptates voluptatem nobis. Velit sit labore
              voluptatem veniam quaerat dolore earum expedita. Laudantium illum
              repellat quod cum.
            </StyledDesignDescription>
            <GetStartedButton type="primary">Get Started</GetStartedButton>
          </Space>
          <Space direction="vertical">
            <StyledCommentText>
              &quot;Exercitationem soluta asperiores. Dignissimos veritatis
              repellendus odit quaerat iusto sed eum. Aut hic quis ea possimus
              et omnis facilis quibusdam architecto.&quot;
            </StyledCommentText>
            <StyledIntroSection>
              <Avatar src="https://joeschmoe.io/api/v1/random" />
              <StyledNameDesgination>
                Marcia Hill, Digital Marketing Manager
              </StyledNameDesgination>
            </StyledIntroSection>
          </Space>
        </StyledSpace>
        <StyledRightDesignDiv></StyledRightDesignDiv>
      </StyledContainer>
      <StyledContainer>
        <StyledLeftDesignDiv></StyledLeftDesignDiv>
        <StyledSpace direction="vertical">
          <BankTwoTone style={{ fontSize: '24px' }} twoToneColor="#874ed2" />
          <StyledHeading>Better understand you customers</StyledHeading>
          <StyledDesignDescription>
            Eveniet dolorum unde reprehenderit libero accusamus quaerat
            doloribus iure quidem. Ullam libero iure magnam quis. Sint rerum sit
            unde dicta dolor omnis eos. Maiores quia sunt minus est consequatur
            atque maiores. Iusto eveniet asperiores est praesentium quisquam
            harum placeat consequuntur. Facere inventore repellat sed labore
            voluptas rem.
          </StyledDesignDescription>
          <GetStartedButton type="primary">Get Started</GetStartedButton>
        </StyledSpace>
      </StyledContainer>
    </StyledDesignSection>
  );
};
