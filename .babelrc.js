module.exports = {
  presets: [['next/babel']],
  plugins: [
    ['import', { libraryName: 'antd', style: true }],
    [
      'module-resolver',
      {
        alias: {
          '@/components': './components',
          '@/types': './types',
        },
      },
    ],
    ['styled-components', { ssr: true }],
  ],
};
