import { Button, Space } from 'antd';
import styled from 'styled-components';

export const StyledDesignSection = styled.section``;

export const StyledContainer = styled.div`
  display: flex;
  margin-top: 40px;
  justify-content: space-between;

  & > .ant-space {
    width: 45%;
  }
`;

export const StyledRightDesignDiv = styled.div`
  margin-right: -30px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  background-image: linear-gradient(to right, #7c49cf, violet);
  height: 450px;
  width: calc(45% + 30px);
`;

export const StyledLeftDesignDiv = styled.div`
  margin-left: -30px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  background-image: linear-gradient(to right, #7c49cf, violet);
  height: 450px;
  width: calc(45% + 30px);
`;

export const StyledSpace = styled(Space)`
  justify-content: center;
`;

export const GetStartedButton = styled(Button)`
  background-image: linear-gradient(to right, #773beb, #5045e5);
  border: 0;
  border-radius: 5px;
`;

export const StyledHeading = styled.h1`
  font-weight: bold;
  font-size: 38px;
`;

export const StyledDesignDescription = styled.p`
  color: #717480;
  font-size: 18px;
`;

export const StyledCommentText = styled.p`
  color: #717480;
  font-size: 16px;
  font-weight: 300;
`;

export const StyledIntroSection = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledNameDesgination = styled.p`
  margin: 0;
  margin-left: 10px;
  font-size: 18px;
`;
