import { Button } from 'antd';
import styled from 'styled-components';

export const StyledGetStartedSection = styled.section`
  display: flex;
  justify-content: space-between;
  padding: 60px 30px;
`;

export const StyledLeftContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
`;

export const StyledRightContainer = styled.div`
  display: flex;
  width: 50%;
  justify-content: end;
`;

export const StyledHeaderText = styled.h1`
  font-size: 42px;
  line-height: 48px;
  font-weight: bold;
  margin: 0;
  color: #5646e5;
`;

export const LearnMoreButton = styled(Button)`
  background-image: linear-gradient(to right, #773beb, #5045e5);
  border: 0;
  border-radius: 5px;
  color: white;
`;

export const GetStartedButton = styled(Button)`
  background-color: #eef2fe;
  border: 0;
  border-radius: 5px;
  color: #453a9b;
`;
