import {
  StyledContainer,
  StyledHeaderText,
  StyledMetric,
  StyledMetrics,
  StyledMetricsSection,
  StyledRegularText,
  StyledSubHeaderText,
} from './elements';
import { Space } from 'antd';

export const MetricsSection: React.FC = () => {
  return (
    <StyledMetricsSection>
      <StyledContainer>
        <StyledSubHeaderText>VALUABLE METRICS</StyledSubHeaderText>
        <StyledHeaderText>
          Get actionable data that will help grow your business
        </StyledHeaderText>
        <StyledRegularText>
          Vel aliquam est accusamus animi esse sit aut accusamus quia. Deserunt
          non nam facilis. Repellat dignissimos ex eos nostrum qui quae vitae.
          Id nam sint quaerat. Et cum facere est voluptate tempora fuga eos
          dicta sed. Ipsum quia sed porro qui sapiente.
        </StyledRegularText>
        <Space direction="vertical">
          <StyledMetrics>
            <StyledMetric>
              <StyledHeaderText>8K+</StyledHeaderText>
              <StyledRegularText>
                <span style={{ color: 'white' }}>Compaines</span>
                &nbsp;enim quis doloremque.
              </StyledRegularText>
            </StyledMetric>
            <StyledMetric>
              <StyledHeaderText>25K+</StyledHeaderText>
              <StyledRegularText>
                <span style={{ color: 'white' }}>
                  Countries around the globe
                </span>
                &nbsp;veritatis et distinctio maxime voluptatem.
              </StyledRegularText>
            </StyledMetric>
            <StyledMetric>
              <StyledHeaderText>98%</StyledHeaderText>
              <StyledRegularText>
                <span style={{ color: 'white' }}>Customer satisfaction</span>
                &nbsp;autem qui reiciendis suscipit facere.
              </StyledRegularText>
            </StyledMetric>
            <StyledMetric>
              <StyledHeaderText>12M+</StyledHeaderText>
              <StyledRegularText>
                <span style={{ color: 'white' }}>Issues resolved</span>
                &nbsp;consequatur temporibus optio.
              </StyledRegularText>
            </StyledMetric>
          </StyledMetrics>
        </Space>
      </StyledContainer>
    </StyledMetricsSection>
  );
};
